// insertOne Method

db.hotel.insert(
{
name: "single",
accommodates: 2,
price: 1000,
description: "A simple room with all the basic necessities",
rooms_available: 10,
isAvailable: false
}
);

//insertMany Method

db.hotel.insertMany(
[
  {
  name: "double",
  accommodates: 3,
  price: 2000,
  description: "A room fit for a small family going on a vacation",
  rooms_available: 5,
  isAvailable: false
},
{
  name: "queen",
  accommodates: 4,
  price: 4000,
  description: "A room with a queen sized bed perfect for a simple getaway",
  rooms_available: 15,
  isAvailable: false
}
]
);

// search double room using find method

db.hotel.find({ name: "double" });

//update queen room using updateOne method


db.hotel.updateOne(
{
name: "queen"
},
{
$set: {
rooms_available: 0
}
}
);

//delete rooms with 0 availability using deleteMany method

db.hotel.deleteMany({rooms_available: 0});